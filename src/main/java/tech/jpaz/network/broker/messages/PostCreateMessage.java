package tech.jpaz.network.broker.messages;

import java.util.UUID;

public record PostCreateMessage(
        UUID id,
        String title,
        String resume,
        UUID author
) {
}
