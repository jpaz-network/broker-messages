package tech.jpaz.network.broker.messages;

import java.util.UUID;

public record AccountCreateMessage(
        UUID id,
        String username,
        String email,
        String displayName,
        String avatarUrl
) {
}
